package controller;

import java.net.UnknownHostException;

import com.mongodb.Mongo;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.daofacade.ComposedObjectDaoFactory;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.Question;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.model.implementation.user.User;
import fr.esiea.pair.model.implementation.user.composant.Authority;

public class Fill {
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 * @throws NoDataBaseConnectionException 
	 */
	public static void main(String[] args) throws UnknownHostException, NoDataBaseConnectionException {
		
		Mongo m;
		
		DaoFacade<IUser> userDao;
		DaoFacade<IQuestion> questionDao; 
		
		IUser cecile, damien, soraya, maxence;
		
		Question question1,question2,question3;
		String titleQuestion1,titleQuestion2,titleQuestion3;
		String textQuestion1,textQuestion2,textQuestion3;
		Answer answer1Q1,answer1Q2,answer2Q2,answer1Q3,answer2Q3,answer3Q3;
		String answer1Q1text,answer1Q2text,answer2Q2text,answer1Q3text,answer2Q3text,answer3Q3text;
		Vote vote1;
		
		userDao = ComposedObjectDaoFactory.getUserDaoImplementation();
		questionDao = ComposedObjectDaoFactory.getQuestionDaoImplementation();
		
		cecile = new User("Cecile D.", "pwd");
		soraya = new User("Soraya B.", "pwd");
		damien = new User("Damien D.", "pwd");
		maxence = new User("Maxence L.","pwd");
		
		cecile.addAuthority(new Authority("ROLE_USER"));
		soraya.addAuthority(new Authority("ROLE_USER"));
		damien.addAuthority(new Authority("ROLE_USER"));
		maxence.addAuthority(new Authority("ROLE_USER"));
		//Question1
		titleQuestion1 	= "Fill Humour noir";
		textQuestion1 	= "L'inventeur du mot 'Tumeur' voulait-il faire de l'humour noir ?";
		answer1Q1text 	= "No comment";
		Tag tagQ1			= new Tag("Humour","funny");
		//Question2
		titleQuestion2 	= "Bored";
		textQuestion2 	= "Faut-il s'armer de patience pour tuer le temps ?";
		answer1Q2text 	= "Shut up";
		answer2Q2text 	= "A defaut de tuer le temps, travaille!";
		Tag tag1Q2			= new Tag("Bored","description bored");
		Tag tag2Q2			= new Tag("Time","bored time");
		//Question3
		titleQuestion3 	= "Windows";
		textQuestion3 	= "Pourquoi la fonction 'ajout/suppression de programmes' sous Windows ne sert elle qu'a supprimer les programmes?";
		answer1Q3text 	= "Parceque c'est windows";
		answer2Q3text	= "Solution : quitte windows!";
		answer3Q3text   = "Un windows qui ne plante pas, c'est un windows ou les buggs ont plante";
		//Questions
		question1 = new Question(cecile,titleQuestion1,textQuestion1);
		question2 = new Question(damien,titleQuestion2,textQuestion2);
		question3 = new Question(soraya,titleQuestion3,textQuestion3);
		questionDao.insert(question1);
		questionDao.insert(question2);
		questionDao.insert(question3);
		//Answers
		answer1Q1 = new Answer(damien,answer1Q1text,question1);
		answer1Q2 = new Answer(cecile,answer1Q2text,question2);
		answer2Q2 = new Answer(maxence,answer2Q2text,question2);
		answer1Q3 = new Answer(damien,answer1Q3text,question3);
		answer2Q3 = new Answer(soraya,answer2Q3text,question3);
		answer3Q3 = new Answer(maxence,answer3Q3text,question3);		
		
		//Add answers
		question1.addAnswer(answer1Q1);
		question2.addAnswer(answer1Q2);
		question2.addAnswer(answer2Q2);
		question3.addAnswer(answer1Q3);
		question3.addAnswer(answer2Q3);
		question3.addAnswer(answer3Q3);
		
		question1.addTag(tagQ1);
		question2.addTag(tag1Q2);
		question2.addTag(tag2Q2);
		
		vote1 = new Vote(damien, question1.getId(), 1);
		question1.addVote(vote1);

		//Erase the database 
		m = new Mongo();

		for (String s : m.getDatabaseNames()) {
			m.dropDatabase(s);
		}

		userDao.insert(cecile);
		userDao.insert(soraya);
		userDao.insert(damien);
		userDao.insert(maxence);
		

		questionDao.save(question1);
		questionDao.save(question2);
		questionDao.save(question3);
		
		
	}

}
