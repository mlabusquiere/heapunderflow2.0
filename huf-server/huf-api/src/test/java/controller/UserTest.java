package controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mongodb.Mongo;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.daofacade.ComposedObjectDaoFactory;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.Question;
import fr.esiea.pair.model.implementation.user.User;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


//HELP to make test
//
//MockMvc plus ou moin simule le conteneur de servlets : pour nous tomcat 
//
//Debugg use andDo(print()); as below
//this.mockMvc.perform(get("/users/").accept(MediaType.APPLICATION_JSON)).andDo(print());
//to use uncomment the import
//
//All reference about jsonPath here : http://goessner.net/articles/JsonPath/

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml"})
@WebAppConfiguration
public class UserTest {//implements ControllerTest{

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	private DaoFacade<IUser> userDao;
	private DaoFacade<IQuestion> questionDao; 
	private DaoFacade<Answer> answerDao;
	private IUser cecile, damien, soraya;
	private IQuestion q1, q2, q3, q4, q5;
	private Answer a1, a2, a3, a4, a5;

	@Before
	public void setup() throws NoDataBaseConnectionException, UnknownHostException {

		mockMvc =  MockMvcBuilders.webAppContextSetup(wac).build();
		
		//Erase the database 
		Mongo m;

		m = new Mongo();
		
		for (String s : m.getDatabaseNames()) {
			m.dropDatabase(s);
		}
		
		userDao = ComposedObjectDaoFactory.getUserDaoImplementation();
		questionDao = ComposedObjectDaoFactory.getQuestionDaoImplementation();
		answerDao = ComposedObjectDaoFactory.getAnswerDaoImplementation();
		
		cecile = new User("Cecile D.", "pwd");
		soraya = new User("Soraya B.", "pwd");
		damien = new User("Damien D.", "pwd");
		
		q1 = new Question(damien, "Question 1", "q1");
		q2 = new Question(damien, "Question 2", "q2");
		q3 = new Question(cecile, "Question 3", "q3");
		q4 = new Question(cecile, "Question 4", "q4");
		q5 = new Question(soraya, "Question 5", "q5");
		
		a1 = new Answer(cecile, "a1", q1);
		a2 = new Answer(damien, "a2", q1);
		a3 = new Answer(soraya, "a3", q2);
		a4 = new Answer(soraya, "a4", q1);
		a5 = new Answer(cecile, "a5", q3);
		

		q1.addAnswer(a1);
		q1.addAnswer(a2);
		q2.addAnswer(a3);
		q1.addAnswer(a4);
		q3.addAnswer(a5);

		userDao.insert(cecile);
		userDao.insert(soraya);
		userDao.insert(damien);

		questionDao.insert(q1);
		questionDao.insert(q2);
		questionDao.insert(q3);
		questionDao.insert(q4);
		questionDao.insert(q5);
		
		answerDao.insert(a1);
		answerDao.insert(a2);
		answerDao.insert(a3);
		answerDao.insert(a4);
		answerDao.insert(a5);
	}


	@After
	public void tearDown() throws UnknownHostException {
		
//		Mongo m;
//
//		m = new Mongo();
//		
//
//		for (String s : m.getDatabaseNames()) {
//			m.dropDatabase(s);
//		}
	}

	//@Override
	@Test
	public void test() throws Exception {
	
		//TEST GET /users
		
		this.mockMvc.perform(get("/users").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.[*].pseudo", hasItems(damien.getPseudo(), soraya.getPseudo(),cecile.getPseudo() )))
			.andExpect(jsonPath("$.[*].password", hasItems(damien.getPassword(), soraya.getPassword(),cecile.getPassword() )))	
			.andExpect(jsonPath("$.[*]._id", hasItems(damien.getId(), soraya.getId(),cecile.getId() )));
			
		//TEST GET /users/{id} 
		
		this.mockMvc.perform(get("/users/"+damien.getId()).accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.pseudo").value(damien.getPseudo()))
			.andExpect(jsonPath("$._id").value(damien.getId()))
			.andExpect(jsonPath("$.password").value(damien.getPassword()));
		
		///TEST POST /users
		
		User maxence = new User("Maxence L.","pwd");
		maxence.setId(null);

		this.mockMvc.perform(post("/users")
			.content(maxence.toJsonString().getBytes())
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated());
		
		Iterable<IUser> users = userDao.getSome("{pseudo: 'Maxence L.'}");
		assertThat(users.iterator().hasNext(),is(true));
		
		//TEST PUT /users 
		
		damien.setPseudo("Lutin DD");
		
		this.mockMvc.perform(put("/users")
			.content(damien.toJsonString().getBytes())
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNoContent());
		
		 assertThat(userDao.getOne(new ObjectId(damien.getId())),equalTo(damien));
		
		//DELETE /users/{id}
		
		this.mockMvc.perform(delete("/users/"+soraya.getId())
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNoContent());
		
		 assertThat(userDao.getOne(new ObjectId(soraya.getId())),is(nullValue()));
		 
		
		//TODO a Bouger dans un autre test
		//GET users/{id}/questions
		//Retourne les questions postées par l’utilisateur correspondant à l’id

		//TODO a Bouger dans un autre test
		//GET users/{id}/answers
		//Retourne les réponses[a] postées par l’utilisateur correspondant à l’id
		
		////TODO test Erreur
		 
	}
}



