package fr.esiea.pair.service.exception.implementation.format;



@SuppressWarnings("serial")
public class WrongUserFormatException extends WrongFormatException {

	public WrongUserFormatException(String message) {
		super(400, 10027, null,"/rest/error.jsp", message);
	}

	public WrongUserFormatException() {
		super(400, 10027, null,"/rest/error.jsp", null);
	}

}
