package fr.esiea.pair.service.manager.imanager;

import org.bson.types.ObjectId;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;
import fr.esiea.pair.service.exception.implementation.format.WrongFormatException;

interface IDaoManager<T extends IModel> {
	
	public void create(T model) throws DaoException, WrongFormatException;

	public void update(T model) throws DaoException, WrongFormatException, InvalidObjectIdException;

	public void delete(ObjectId id) throws DaoException, InvalidObjectIdException;

}
