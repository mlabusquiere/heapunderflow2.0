package fr.esiea.pair.api.controller;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.esiea.pair.api.controller.exception.implementation.NeedToBeAuthenticatedException;
import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.user.User;
import fr.esiea.pair.model.implementation.user.composant.Authority;
import fr.esiea.pair.service.exception.ServiceException;
import fr.esiea.pair.service.manager.imanager.IManager;
import fr.esiea.pair.service.manager.implementation.UserManager;


@Controller
@RequestMapping("/users")
public class UserController extends ApiExceptionHandlerController{

	private IManager<IUser> userManager;

	public UserController() throws DaoException {
		//logger.info("[Controller] API - /UserController()");
		userManager = ManagerFactory.getUserManager();
	}
	
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Iterable<IUser> list() throws DaoException {
		logger.info("[Controller] Querying User list");
		return userManager.list();
	
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public IModel getById(@PathVariable("id") String userId) throws ServiceException, DaoException {   
		logger.info("[Controller] Querying User with id : \""+userId+"\"");
		return userManager.get(userId); 

	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void create(@RequestBody User user) throws ServiceException, DaoException {
		logger.info("[Controller] Querying to create new User : \""+user.toString()+"\"");
		userManager.create(user);

	} 
	
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@RequestMapping(value  = "", method = RequestMethod.PUT,consumes = "application/json")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void edit(@RequestBody IUser user) throws NeedToBeAuthenticatedException, ServiceException, DaoException {
		logger.info("[Controller] Querying to edit User : \""+user.toString()+"\" (Request from User : \""+getUserConnected().getId()+"\")");
		if(getUserConnected().getId().equals(user.getId()) || getAuthorities("ROLE_ADMIN"))
			userManager.update(user);
		//TODO may else we should send an error 
	} 


	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@RequestMapping(value = "/{idUser}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String idUser) throws NeedToBeAuthenticatedException, ServiceException, DaoException {
		logger.info("[Controller] Querying to delete User with id : \""+idUser+"\" (Request from User : \""+getUserConnected().getId()+"\")");
		if(getUserConnected().getId().equals(idUser) || getAuthorities("ROLE_ADMIN"))
			userManager.delete(new ObjectId(idUser));

	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/admin/{idUser}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void MakeAdmin(@PathVariable String idUser) throws ServiceException, DaoException, NeedToBeAuthenticatedException {
		logger.info("[Controller] Querying to change Authority of User with id : \""+idUser+"\" (Request from User : \""+getUserConnected().getId()+"\")");
		((UserManager)userManager).addAuthority(idUser, new Authority("ROLE_ADMIN"));
		
	
	}
	
//	@RequestMapping(value = "/{userId}/questions", method = RequestMethod.GET, produces = "application/json")
//	@ResponseBody
//	public Iterable<Question> getQuestions(@PathVariable String userId)	{
//				logger.info("[Controller] API - /User - getQuestions() user id : "+userId);
//		//TODO
//		//return userManager.getQuestionFrom(new ObjectId(userId));
//
//	}
//
//	@RequestMapping(value = "/{userId}/answers", method = RequestMethod.GET, produces = "application/json")
//	@ResponseBody
//	public Iterable<Answer> getAnswers(@PathVariable String userId) {
//				logger.info("[Controller] API - /User - getAnswers() user id : "+userId);
//		//TODO
//		//return userManager.getAnswersFrom(new ObjectId(userId));
//
//	}

}
