package fr.esiea.pair.api.controller.exception;

import fr.esiea.pair.model.exception.RestException;

@SuppressWarnings("serial")
public class ControllerException extends RestException {

	public ControllerException(int status, int code, String developerMsg, String moreInfoUrl, String msg) {
		super(status, code, developerMsg, moreInfoUrl, msg);
	}

}
