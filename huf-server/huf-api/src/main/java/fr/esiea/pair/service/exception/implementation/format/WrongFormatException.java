package fr.esiea.pair.service.exception.implementation.format;

import fr.esiea.pair.service.exception.ServiceException;

@SuppressWarnings("serial")
public abstract class WrongFormatException extends ServiceException {

	public WrongFormatException(int status, int code, String developerMsg, String moreInfoUrl, String msg) {
		super(status, code, developerMsg, moreInfoUrl, msg);
	}

}