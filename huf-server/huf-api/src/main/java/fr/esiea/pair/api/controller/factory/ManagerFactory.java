package fr.esiea.pair.api.controller.factory;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.dao.mongo.implementation.DaoSimple;
import fr.esiea.pair.daofacade.ComposedObjectDaoFactory;
import fr.esiea.pair.daofacade.SimpleObjectDaoFactory;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.service.manager.implementation.AnswerManager;
import fr.esiea.pair.service.manager.implementation.QuestionManager;
import fr.esiea.pair.service.manager.implementation.TagManager;
import fr.esiea.pair.service.manager.implementation.UserManager;

public class ManagerFactory {

	public static UserManager getUserManager() throws DaoException {
		return new UserManager(ComposedObjectDaoFactory.getUserDaoImplementation(), ComposedObjectDaoFactory.getQuestionDaoImplementation(), ComposedObjectDaoFactory.getAnswerDaoImplementation());
	}

	@SuppressWarnings("unchecked")
	public static TagManager getTagManger() throws DaoException {
		return new TagManager((DaoSimple<Tag>)SimpleObjectDaoFactory.getImplementation(MongoCollections.TAGS));
	}

	public static QuestionManager getQuestionManger() throws DaoException {
		return  new QuestionManager(ComposedObjectDaoFactory.getQuestionDaoImplementation(),ComposedObjectDaoFactory.getUserDaoImplementation());	    
	}

	public static AnswerManager getAnswerManager() throws DaoException {
		return new AnswerManager(ComposedObjectDaoFactory.getAnswerDaoImplementation(), ComposedObjectDaoFactory.getQuestionDaoImplementation(),ComposedObjectDaoFactory.getUserDaoImplementation());
	          
	}

		
}
