package fr.esiea.pair.service.manager.imanager;

import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.service.manager.imanager.implementation.IViewManager;

public abstract class IManager<T extends IModel> extends IViewManager<T> implements IDaoManager<T> {

	public IManager(DaoFacade<T> dao) {
		super(dao);
	}

}
