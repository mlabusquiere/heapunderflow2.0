package fr.esiea.pair.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.service.manager.imanager.implementation.IViewManager;

@Controller
@RequestMapping("/tags")
public class TagController extends ApiExceptionHandlerController{

	private IViewManager<Tag> tagManager;

	public TagController() throws DaoException {
		//logger.info("[Controller] API - /TagController()");
		tagManager = ManagerFactory.getTagManger();
	
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Iterable<Tag> list() throws DaoException {
		logger.info("[Controller] Querying Tag list");
		return tagManager.list();
	}

}
