
package fr.esiea.pair.api.controller.exception.implementation;

import fr.esiea.pair.api.controller.exception.ControllerException;

@SuppressWarnings("serial")
public class NeedToBeAuthenticatedException extends ControllerException {
	//TODO correct the message
	public NeedToBeAuthenticatedException(String message) {
		super(400, 10025, null,"/rest/error.jsp", message);
	}

	public NeedToBeAuthenticatedException() {
		super(400, 10025, null,"/rest/error.jsp", null);
	}

}
