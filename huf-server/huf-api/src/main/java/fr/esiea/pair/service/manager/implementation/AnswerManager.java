package fr.esiea.pair.service.manager.implementation;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;
import fr.esiea.pair.service.exception.implementation.format.WrongAnswerFormatException;

public class AnswerManager extends PostManager<Answer> {

    private DaoFacade<IQuestion> questionDao;
	private DaoFacade<Answer> answerDao;
	public static Logger logger = Logger.getLogger(AnswerManager.class);


	public AnswerManager(DaoFacade<Answer> answerDao, DaoFacade<IQuestion> questionDao,DaoFacade<IUser> userDao) throws DaoException {
		super(userDao,answerDao);
		this.answerDao = answerDao;
		this.questionDao = questionDao;
    }

    public void create(Answer answer) throws DaoException, WrongAnswerFormatException {
    	logger.info("[Core] Managing to create Answer : \""+answer.toString()+"\"");
        checkAnswer(answer);

        if (answer.getId() != null) {
            throw new WrongAnswerFormatException("If you want to update a answer use PUT");
        }

        IQuestion question = questionDao.getOne(new ObjectId(answer.getRefQuestion()));
        question.addAnswer(answer);
        questionDao.save(question);

    }
    @Override
    public void update(Answer answer) throws DaoException, WrongAnswerFormatException {
    	logger.info("[Core] Managing to update Answer : \""+answer.toString()+"\"");
    	checkAnswer(answer);
        if (answer.getId() == null) {
            throw new WrongAnswerFormatException("If you want to create a answer use POST");
        }
        answerDao.save(answer);
    }

    public void delete(ObjectId answerId) throws DaoException, InvalidObjectIdException {
    	logger.info("[Core] Managing to delete Answer with id : \""+answerId.toString()+"\"");
        try {
            answerDao.remove(answerId);
        } catch (IllegalArgumentException e) {
            throw new InvalidObjectIdException("Invalid id");
        }

    }
    
    protected Iterable<Answer> getAnswerFrom(ObjectId authorId) throws DaoException, InvalidObjectIdException {
        //use by getAnswerFrom in answerManager
    	logger.info("[Core] Managing to get Answers from user \""+authorId.toString()+"\"");
        
        Iterable<Answer> answers;

        try {
            answers = answerDao.getSome("userId", authorId);
            if (!answers.iterator().hasNext()) {
                throw new InvalidObjectIdException(authorId);
            }
        } catch (IllegalArgumentException e) {
            throw new InvalidObjectIdException("Invalid id");
        }
        return answers;
    }

    protected Iterable<Answer> getAnswerFrom(ObjectId authorId, int skip, int limit) throws DaoException, InvalidObjectIdException {
        //use by getAnswerFrom in answerManager
    	logger.info("[Core] Managing to get Answers from user \""+authorId.toString()+"\" starting at : \""+skip+"\" limit : \""+limit+"\"");
    
        Iterable<Answer> answers;

        try {
            answers = answerDao.getSome("userId", authorId);
            if (!answers.iterator().hasNext()) {
                throw new InvalidObjectIdException(authorId);
            }
        } catch (IllegalArgumentException e) {
            throw new InvalidObjectIdException("Invalid id");
        }
        return answers;
    }

    /**
     * Private methods
     *
     * @throws WrongAnswerFormatException **
     */

    private void checkAnswer(Answer answer) throws WrongAnswerFormatException {
    	logger.info("[Core] Checking Answer : \""+answer.toString()+"\"");
        /*
		logger.info("SERVICE - AnswerManager - checkAnswer()  "+answer.toString());
    
		WrongAnswerFormatException e = new WrongAnswerFormatException();
		
		if(answer.getUserId()	==	null)	
			e.add("Missing userId");
		if(answer.getUserPseudo()	==	null)
			e.add("Missing UserPseudo");
		if(answer.getTitle()	==	null)
			e.add("g Title");
		if(answer.getText()	==	null)
			e.add("Missing Text");
		
		if(e.isThereAnError())	{
			throw e;
		}
         */
    }

}
