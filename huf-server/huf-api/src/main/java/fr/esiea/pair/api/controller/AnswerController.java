package fr.esiea.pair.api.controller;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.esiea.pair.api.controller.exception.implementation.NeedToBeAuthenticatedException;
import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.service.exception.ServiceException;
import fr.esiea.pair.service.manager.imanager.IPostManager;

@Controller
@RequestMapping("/answers")
public class AnswerController extends ApiExceptionHandlerController {

	private IPostManager<Answer> answerManager;
    public AnswerController() throws DaoException {
		//logger.info("[Controller] API - /AnswerController()");
        answerManager = ManagerFactory.getAnswerManager();
    }
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/{refQuestion}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@PathVariable String refQuestion, @RequestBody String text) throws NeedToBeAuthenticatedException, DaoException,ServiceException {
    	logger.info("[Controller] Querying to add Answer : \""+text+"\" to the Question with id : \""+refQuestion+"\" (Request from User : \""+getUserConnected().getId()+"\")");
		System.out.println("Try to add a answer : "+text);
    	Answer answer = new Answer(getUserConnected(), text, refQuestion);
    	answerManager.create(answer);
    }
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/{id}/vote", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addVote(@RequestBody int weight, @PathVariable String id) throws  NeedToBeAuthenticatedException, ServiceException, DaoException {   
    	logger.info("[Controller] Querying to add Vote with the value of : \""+weight+"\" to the Answer with id : \""+id+"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	int coeff=1;
    	if(weight<0)
    		coeff=-1;
    	Vote vote = new Vote(getUserConnected(),id,coeff);
    	
    	answerManager.addVote(getUserConnected(), vote);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public void Answer(@PathVariable String id) throws  NeedToBeAuthenticatedException, ServiceException, DaoException {   
		logger.info("[Controller] Querying to get question with the id "+id);
    	answerManager.get(new ObjectId(id));
    }
    
}
