package fr.esiea.pair.service.exception.implementation;

import fr.esiea.pair.service.exception.ServiceException;

@SuppressWarnings("serial")
public class AlreadyVotedException extends ServiceException {
	//TODO check the exception
	public AlreadyVotedException() {
		super(403,10029, null,"/rest/error.jsp","You already voted");
	}

}
