package fr.esiea.pair.service.exception;

import fr.esiea.pair.model.exception.RestException;

@SuppressWarnings("serial")
public class ServiceException extends RestException {

	public ServiceException(int status, int code, String developerMsg, String moreInfoUrl, String msg) {
		super(status, code, developerMsg, moreInfoUrl, msg);
	}

}
