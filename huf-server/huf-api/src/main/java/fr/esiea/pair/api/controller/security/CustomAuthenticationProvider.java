package fr.esiea.pair.api.controller.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.ComposedObjectDaoFactory;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.user.composant.Authority;
import fr.esiea.pair.service.manager.implementation.UserManager;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	private UserDetailsService userDetailsService; //what am i supposed to do with this?
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
		
		String pseudo = String.valueOf(auth.getPrincipal());
		String password = String.valueOf(auth.getCredentials());
		
		UserManager userManager;
		IUser userToAuthenticate;
		
		try {
			
			userManager = new UserManager(ComposedObjectDaoFactory.getUserDaoImplementation(), ComposedObjectDaoFactory.getQuestionDaoImplementation(), ComposedObjectDaoFactory.getAnswerDaoImplementation());
			userToAuthenticate = userManager.getUserByPseudo(pseudo);

			if(userToAuthenticate == null)
				throw new BadCredentialsException("Bad Login");
			
			if(!userToAuthenticate.getPassword().equals(password))
				throw new BadCredentialsException("Bad password");

			//Authorities
			Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		    
			Set<IModel> authoritiesAsModel = userToAuthenticate.getAuthorities();
		 	for(IModel auhorityModel : authoritiesAsModel)	{
		 		Authority authority = (Authority) auhorityModel;
		 		authorities.add(new SimpleGrantedAuthority(authority.getAuthority()));
		 	}
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(pseudo,password,authorities);
			
			token.setDetails(userToAuthenticate.getId());
			
			return token;
			
		} catch (DaoException e) {
			System.out.println(e.getMessage());
		}
			
		return null;  //what do i return?
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return true;  //To indicate that this authenticationprovider can handle the auth request. since there's currently only one way of logging in, always return true
	}

	public UserDetailsService getUserDetailsService() {
		return userDetailsService;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
}