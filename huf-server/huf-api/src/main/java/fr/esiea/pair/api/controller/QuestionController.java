package fr.esiea.pair.api.controller;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.esiea.pair.api.controller.exception.implementation.NeedToBeAuthenticatedException;
import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Question;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.model.implementation.user.User;
import fr.esiea.pair.service.exception.ServiceException;
import fr.esiea.pair.service.manager.imanager.IPostManager;
import fr.esiea.pair.service.manager.implementation.QuestionManager;

@Controller
@RequestMapping("/questions")
public class QuestionController extends ApiExceptionHandlerController {

    private IPostManager<IQuestion> questionManager;
//    private static Logger logger = Logger.getLogger(QuestionController.class);

    public QuestionController() throws DaoException {
    	//logger.info("[Controller] QuestionsController()");
        questionManager = ManagerFactory.getQuestionManger();
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Iterable<IQuestion> list() throws DaoException {
    	logger.info("[Controller] Querying Question list");
        return questionManager.list();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public IQuestion getById(@PathVariable("id") String questionId) throws DaoException, ServiceException, JsonProcessingException {
    	logger.info("[Controller] Querying Question with id : \""+questionId+"\"");
    	return questionManager.get(questionId);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Question question) throws  NeedToBeAuthenticatedException, DaoException, ServiceException {
    	logger.info("[Controller] Querying to create Question : \""+question.toString()+"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	User author = (User)getUserConnected();
    	question.setAuthorId(author.getId());
    	question.setAuthorPseudo(author.getPseudo());
        questionManager.create(question);
    }

    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void edit(@RequestBody Question question, @PathVariable String id) throws  NeedToBeAuthenticatedException, DaoException, ServiceException {
    	logger.info("[Controller] Querying to update Question with id : \""+id+"\" : \""+question.toString()+"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	if(getUserConnected().getId() == questionManager.get(id).getAuthorId() || getAuthorities("ROLE_ADMIN"))
        	questionManager.update(question);

    }
    
    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) throws  NeedToBeAuthenticatedException, DaoException, ServiceException {
    	logger.info("[Controller] Querying to delete Question with id : \""+id+"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	if(getUserConnected().getId() == questionManager.get(id).getAuthorId() || getAuthorities("ROLE_ADMIN"))
    		questionManager.delete(new ObjectId(id));
    	
    }

    @RequestMapping(value = "/{id}/author", method = RequestMethod.GET)
    public User getAuthor(@PathVariable String id) {
    	logger.info("[Controller] Querying author of the Question with id : \""+id+"\"");
        // TODO
		return null;
    }

    @RequestMapping(value = "/{id}/answers", method = RequestMethod.GET)
    public void getAnswers(@PathVariable String id) {
    	logger.info("[Controller] Querying Answers of the Question with id : \""+id+"\"");
        // TODO
    }

    @RequestMapping(value = "/{id}/title", method = RequestMethod.GET)
    public void getTitle(@PathVariable String id) {
    	logger.info("[Controller] Querying title of the Question with id : \""+id+"\"");
        // TODO
    }
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/{id}/vote", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addVote(@RequestBody int weight, @PathVariable String id) throws  NeedToBeAuthenticatedException, ServiceException, DaoException {
    	logger.info("[Controller] Querying to add Vote with value the of : \""+weight+"\" to the Question with id : \""+id+"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	int coeff=1;
    	if(weight<0)
    		coeff=-1;
    	IUser userAuthenticated = getUserConnected();
    	Vote vote = new Vote(userAuthenticated, id, coeff);
    	((QuestionManager)questionManager).addVote(userAuthenticated, vote);
    }
    
    @Secured({"ROLE_USER","ROLE_ADMIN"})
    @RequestMapping(value = "/{id}/tag", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addTag(@RequestBody Tag tag, @PathVariable String id) throws NeedToBeAuthenticatedException, ServiceException, DaoException {
    	logger.info("[Controller] Querying to add Tag : \""+tag.toString()+"\" to the Question with id : \""+id +"\" (Request from User : \""+getUserConnected().getId()+"\")");
    	
    	if(getUserConnected().getId() == questionManager.get(id).getAuthorId() || getAuthorities("ROLE_ADMIN"))
    		((QuestionManager)questionManager).addATag(tag,id);
    	//TODO may else send back an error
    }
}
