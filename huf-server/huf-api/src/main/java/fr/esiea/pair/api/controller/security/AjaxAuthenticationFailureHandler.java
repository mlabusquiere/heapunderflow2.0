package fr.esiea.pair.api.controller.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import fr.esiea.pair.api.controller.ApiExceptionHandlerController;

public class AjaxAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler{

	public static Logger logger = Logger.getLogger(ApiExceptionHandlerController.class);

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException{
		logger.warn("API security - onAuthenticationFailure()");
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Wrong username or password");
	}
}