package fr.esiea.pair.api.controller;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.esiea.pair.api.controller.exception.implementation.NeedToBeAuthenticatedException;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.ComposedObjectDaoFactory;
import fr.esiea.pair.model.exception.RestException;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;
import fr.esiea.pair.service.manager.implementation.UserManager;

@Controller
public abstract class ApiExceptionHandlerController {

	public static Logger logger = Logger.getLogger(ApiExceptionHandlerController.class);

	@ExceptionHandler(RestException.class)
	@ResponseBody
	public ResponseEntity<RestException> handleException(RestException ex) {

		return new ResponseEntity<RestException>(ex, HttpStatus.valueOf(ex.getStatus()));

	}

	@ExceptionHandler(HttpMessageNotWritableException.class)
	@ResponseBody
	public ResponseEntity<String> InternalServerError(HttpMessageNotWritableException e) {
		//TODO make it json & wtf? Users need that to detect NoDatabaseConecction
		e.printStackTrace();
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

	}

	final protected IUser getUserConnected() throws DaoException, NeedToBeAuthenticatedException	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserManager userManager = new UserManager(ComposedObjectDaoFactory.getUserDaoImplementation(), ComposedObjectDaoFactory.getQuestionDaoImplementation(), ComposedObjectDaoFactory.getAnswerDaoImplementation());
		IUser userAuthenticate = null;

		try {
			userAuthenticate = (IUser) userManager.get(authentication.getDetails().toString());
		} catch (InvalidObjectIdException e) {
			throw new NeedToBeAuthenticatedException();
		} 

		return userAuthenticate;
	}

	final protected boolean getAuthorities(String role) {

		Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		return authorities.contains(new SimpleGrantedAuthority(role));

	}
}
