package fr.esiea.pair.service.manager.imanager;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IPost;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.service.exception.ServiceException;
import fr.esiea.pair.service.exception.implementation.AlreadyVotedException;

public abstract class IPostManager<T extends IPost> extends IManager<T> {

	public IPostManager(DaoFacade<T> dao) {
		super(dao);
	}

	public abstract void addVote(IUser userAuthenticated, Vote vote)
			throws AlreadyVotedException, ServiceException, DaoException;

}