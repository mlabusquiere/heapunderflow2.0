package fr.esiea.pair.api.controller.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import fr.esiea.pair.api.controller.ApiExceptionHandlerController;

public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	public static Logger logger = Logger.getLogger(ApiExceptionHandlerController.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, 
			HttpServletResponse response, 
			Authentication authentication) throws IOException,ServletException {
		logger.info("API security - /onAuthenticationSuccess()");
        response.getWriter().print("Welcome !");
        response.getWriter().flush();
	}
}
