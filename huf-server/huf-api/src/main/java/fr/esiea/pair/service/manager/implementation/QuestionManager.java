package fr.esiea.pair.service.manager.implementation;


import java.util.Iterator;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;
import fr.esiea.pair.service.exception.implementation.format.WrongFormatException;
import fr.esiea.pair.service.exception.implementation.format.WrongQuestionFormatException;

public class QuestionManager extends PostManager<IQuestion>{


	private DaoFacade<IQuestion> questionDao;
	public static Logger logger = Logger.getLogger(QuestionManager.class);


	public QuestionManager(DaoFacade<IQuestion> questionDao, DaoFacade<IUser> userDao) throws NoDataBaseConnectionException {
		super(userDao,questionDao);
		this.questionDao 	= questionDao;
	}

	public void create(IQuestion question) throws NoDataBaseConnectionException, WrongFormatException {
		logger.info("[Core] Managing to create Question : \""+question.toString()+"\"");
		
		checkQuestion(question);

		if(question.getId()!=null)	{
			throw new WrongQuestionFormatException("If you want to update a question use PUT");
		}

		questionDao.save(question);

	}

	public void update(IQuestion question) throws NoDataBaseConnectionException, WrongQuestionFormatException {
		logger.info("[Core] Managing to update Question : \""+question.toString()+"\"");
		
		checkQuestion(question);
		if(question.getId().equals(null))	{
			throw new WrongQuestionFormatException("If you want to create a question use POST");
		}
		questionDao.save(question);
	}

	public void delete(ObjectId questionId) throws NoDataBaseConnectionException, InvalidObjectIdException {
		logger.info("[Core] Managing to delete Question with id \""+questionId.toString()+"\"");
		
		try	{	
			questionDao.remove(questionId);
		} catch(IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

	}

	protected Iterable<IQuestion> getQuestionFrom(ObjectId authorId) throws NoDataBaseConnectionException, InvalidObjectIdException {

		//use by getAnswerFrom in questionManager
		logger.info("[Core] Managing to get Questions from user \""+authorId.toString()+"\"");
		
		Iterable<IQuestion> questions;

		try	{
			questions = questionDao.getSome("userId",authorId);
			if(!questions.iterator().hasNext())	{
				throw new InvalidObjectIdException(authorId);
			}
		}catch(IllegalArgumentException e)	{
			throw new InvalidObjectIdException("Invalid id");
		}
		process(questions);
		return questions;
	}
	
	private void process(Iterable<IQuestion> questions) {

	}

	protected Iterable<IQuestion> getQuestionFrom(ObjectId authorId,int skip,int limit) throws NoDataBaseConnectionException, InvalidObjectIdException {
		//use by getAnswerFrom in questionManager
		logger.info("[Core] Managing to get Questions from user \""+authorId.toString()+"\" starting at : \""+skip+"\" limit : \""+limit+"\"");
		
		Iterable<IQuestion> questions;

		try	{
			questions = questionDao.getSome("userId",authorId);
			if(!questions.iterator().hasNext())	{
				throw new InvalidObjectIdException(authorId);
			}
		}catch(IllegalArgumentException e)	{
			throw new InvalidObjectIdException("Invalid id");
		}
		process(questions);
		return questions;
	}
	
	public void addATag(Tag tag, String id) throws InvalidObjectIdException, DaoException {
		logger.info("[Core] Managing to add Tag \""+tag.toString()+"\" to Question \""+id.toString()+"\"");

		IQuestion question = get(id);
		TagManager tagManager = ManagerFactory.getTagManger();
		
		Iterator<Tag> tagsIterator = tagManager.getByTag(tag.getTag()).iterator();
		if(!tagsIterator.hasNext())
			question.addTag(tag);
		else
			question.addTag(tagsIterator.next());
		//DEBUGG
		if(tagsIterator.hasNext())
			System.out.println("there is a bugg");
		try	{
			update(question);
		}catch(WrongQuestionFormatException e)	{
			System.out.println(" DEBUGG no way that we come here");
		}
		
	}
	/*** Private methods 
	 * @throws WrongFormatException ***/

	private void checkQuestion(IQuestion question) throws WrongQuestionFormatException {
		logger.info("[Core] Checking Question : \""+question.toString()+"\"");

		StringBuilder error = new StringBuilder();

		if(question.getAuthorPseudo() == null) 
			error.append("Null Author Pseudo\n");
		else 
			if(question.getAuthorPseudo().isEmpty()) 
				error.append("Missing Author Pseudo\n");

		if(question.getAuthorId() == null) 
			error.append("No Author Reference\n");

		if(question.getTitle()	==	null)
			error.append("Null Title");
		else 
			if(question.getTitle().isEmpty()) 
				error.append("Missing Title\n");

		if(question.getText()	==	null)
			error.append("Null Text");
		else 
			if(question.getTitle().isEmpty()) 
				error.append("Missing Text\n");

		if(error.length()!=0){
			throw new WrongQuestionFormatException(error.toString());
		}
	}
}
