package fr.esiea.pair.service.manager.imanager.implementation;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;

public abstract class IViewManager<T extends IModel> {
	
	public static Logger logger = Logger.getLogger(IViewManager.class);
	private DaoFacade<T> dao;
	
	public IViewManager(DaoFacade<T> dao)	{
		this.dao =dao;
	}
	public T get(ObjectId modelId) throws DaoException, InvalidObjectIdException {
		logger.info("[core] Getting object with id : "+modelId.toString());
		
		T model;

		try	{	
			model = dao.getOne(modelId);
			if(model == null) {	
				throw new InvalidObjectIdException(modelId);
			}	
		} catch(java.lang.IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

		return model;
	}
	
	public T get(String modelId) throws InvalidObjectIdException, DaoException	{
		return this.get(new ObjectId(modelId));
	}

	public Iterable<T> getQuery(String query) throws DaoException, InvalidObjectIdException {
		logger.info("[core] Getting objects with the query \""+query+"\"");
		
		if(query==null) {
			return this.list();
		}

		Iterable<T> models;

		models = dao.getSome(query);

		return models;
	}

	public Iterable<T> getQuery(String query,int skip,int limit) throws DaoException {
		logger.info("[core] Getting objects with the query \""+query+"\""+query+" skip :"+skip+" limit : "+limit);
		return dao.getSome(query,skip,limit);
	}

	public Iterable<T> list() throws DaoException {
		logger.info("[core] Getting all objects of the dao "+ dao.getClass());
		return dao.getAll();
	}
}
