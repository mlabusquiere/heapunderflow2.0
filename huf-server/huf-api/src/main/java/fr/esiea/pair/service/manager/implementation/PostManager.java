package fr.esiea.pair.service.manager.implementation;

import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IPost;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.service.exception.ServiceException;
import fr.esiea.pair.service.exception.implementation.AlreadyVotedException;
import fr.esiea.pair.service.manager.imanager.IPostManager;

public abstract class PostManager<T extends IPost> extends IPostManager<T>	{

	private DaoFacade<IUser> userDao;

	public static Logger logger = Logger.getLogger(PostManager.class);

	public PostManager(DaoFacade<IUser> userDao, DaoFacade<T> modelDao) {
		super(modelDao);
		this.userDao = userDao;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.service.manager.IPostManager#addVote(fr.esiea.pair.model.imodel.IUser, fr.esiea.pair.model.implementation.post.composant.Vote)
	 */
	@Override
	public void addVote(IUser userAuthenticated, Vote vote) throws AlreadyVotedException, ServiceException,  DaoException {
		logger.info("[Core] Managing to add Vote : \""+vote.toString()+"\"");

		T post = this.get(new ObjectId(vote.getRefPost()));

		Set<IModel> votes = post.getVotes();

		Iterator<IModel> iterator = votes.iterator();
		while(iterator.hasNext())	{
			Vote voteCheck = (Vote) iterator.next();
			if(voteCheck.getAuthorId().equals(userAuthenticated.getId()))
				throw new AlreadyVotedException();
		}
		vote.generateId();

		userAuthenticated.addVote(vote);
		post.addVote(vote);
		
		this.update(post);
		userDao.save(userAuthenticated);
	}
}
