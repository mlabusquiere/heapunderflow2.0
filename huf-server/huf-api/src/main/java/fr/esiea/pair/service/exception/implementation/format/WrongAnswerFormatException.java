package fr.esiea.pair.service.exception.implementation.format;

@SuppressWarnings("serial")
public class WrongAnswerFormatException extends WrongFormatException {

	public WrongAnswerFormatException(String message) {
		super(400, 10025, null,"/rest/error.jsp", message);
	}

	public WrongAnswerFormatException() {
		super(400, 10025, null,"/rest/error.jsp", null);
	}
}
