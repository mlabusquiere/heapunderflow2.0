package fr.esiea.pair.service.manager.implementation;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import fr.esiea.pair.api.controller.factory.ManagerFactory;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.user.composant.Authority;
import fr.esiea.pair.service.exception.implementation.InvalidObjectIdException;
import fr.esiea.pair.service.exception.implementation.format.WrongUserFormatException;
import fr.esiea.pair.service.manager.imanager.IManager;

public class UserManager extends IManager<IUser> {


	private DaoFacade<IUser> userDao;
	private DaoFacade<IQuestion> questionDao; 
	private DaoFacade<Answer> answerDao;
	public static Logger logger = Logger.getLogger(UserManager.class);


	public UserManager(DaoFacade<IUser> userDao, DaoFacade<IQuestion> questionDao, DaoFacade<Answer> answerDao) throws DaoException  {
		super(userDao);
		this.userDao = userDao;
		this.questionDao = questionDao;
		this.answerDao = answerDao;
	}

	public void create(IUser user) throws DaoException, WrongUserFormatException {
		logger.info("[Core] Managing to create User : \""+user.toString()+"\"");
		
		checkUser(user);
		
		//Check if u want to create or update
		if(user.getId()!=null ) 
			throw new WrongUserFormatException("If you want to update an user use PUT");
		
		//Make the pseudo unique
		if(getUserByPseudo(user.getPseudo()) != null)
			throw new WrongUserFormatException("This pseudo already exist");
		
		//Basic authorities
		Authority basicAutoritie = new Authority("ROLE_USER");
		user.addAuthority(basicAutoritie);
		System.out.println("In create");
		userDao.save(user);
	}

	public void update(IUser user) throws DaoException, WrongUserFormatException, InvalidObjectIdException {
		logger.info("[Core] Managing to update User : \""+user.toString()+"\"");
		
		checkUser(user);

		if(user.getId() == null) {
			throw new WrongUserFormatException("If you want to create an user use POST");
		}
		
		//Check if user was already in the database
		IUser checkUser = (IUser) get(user.getId());
		
		if(checkUser == null)
			throw new InvalidObjectIdException(user.getId());
		//Check if the pseudo is still unique
		if(checkUser.getPseudo() != user.getPseudo()  // we are updating the pseudo
				&& getUserByPseudo(user.getPseudo()) != null) //there is another user with this user
			throw new WrongUserFormatException("This pseudo already exist");
		
			
		userDao.save(user);
	}

	public void delete(ObjectId userId) throws DaoException, InvalidObjectIdException {
		logger.info("[Core] Managing to delete User \""+userId.toString()+"\"");
		try	{	
			userDao.remove(userId);
		} catch(IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}
	}
	
	public Iterable<IQuestion> getQuestionFrom(ObjectId userId) throws DaoException, InvalidObjectIdException {
		logger.info("[Core] Managing to get Questions from User \""+userId.toString()+"\"");
		
		QuestionManager questionManager;
		Iterable<IQuestion> questions;

		try	{	
			questionManager = ManagerFactory.getQuestionManger();
			questions = questionManager.getQuestionFrom(userId);
			if(!questions.iterator().hasNext()) {	
				return null;
			}	
		} catch(java.lang.IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

		return questions;

	}

	public Iterable<IQuestion> getQuestionFrom(ObjectId userId, int skip, int limit) throws DaoException, InvalidObjectIdException {
		logger.info("[Core] Managing to get Questions from User \""+userId.toString()+"\" skip : \""+skip+"\" limit : \""+limit+"\"");
		
		QuestionManager questionManager;
		Iterable<IQuestion> questions;

		try	{	
			questionManager = ManagerFactory.getQuestionManger();
			questions = questionManager.getQuestionFrom(userId,skip,limit);
			if(!questions.iterator().hasNext()) {	
				return null;
			}	
		} catch(java.lang.IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

		return questions;

	}

	public Iterable<Answer> getAnswersFrom(ObjectId userId) throws DaoException, InvalidObjectIdException {
		logger.info("[Core] Managing to get Questions from User \""+userId+"\"");
		
		
		AnswerManager answerManager;
		Iterable<Answer> answers;

		try	{	
			answerManager = new AnswerManager(answerDao,questionDao,userDao);
			answers = answerManager.getAnswerFrom(userId);
			if(!answers.iterator().hasNext()) {	
				return null;
			}	
		} catch(java.lang.IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

		return answers;

	}

	public Iterable<Answer> getAnswersFrom(ObjectId userId,int skip,int limit) throws DaoException, InvalidObjectIdException {
		logger.info("[Core] Managing to get Questions from User \""+userId+"\" skip : \""+skip+"\" limit : \""+limit+"\"");
		
		AnswerManager answerManager;
		Iterable<Answer> answers;

		try	{	
			answerManager = new AnswerManager(answerDao,questionDao,userDao);
			answers = answerManager.getAnswerFrom(userId,skip,limit);
			if(!answers.iterator().hasNext()) {	
				return null;
			}	
		} catch(java.lang.IllegalArgumentException e) {
			throw new InvalidObjectIdException("Invalid id");
		}

		return answers;
	}

	/*** Private methods ***/

	private void checkUser(IUser user) throws WrongUserFormatException {
		logger.info("[Core] Checking User : \""+user.toString()+"\"");
		
		StringBuilder error =  new StringBuilder();

		if(user.getPseudo() == null) {
			error.append("Null Pseudo\n");
		}
		else {
			if(user.getPseudo().isEmpty()) {
				error.append("Missing Pseudo\n");
			}
		}

		if(user.getPassword() == null) {
			error.append("Null Password\n");
		}
		else {
			if(user.getPassword().isEmpty()) {
				error.append("Missing Pseudo\n");
			}
		}
		
		if(error.length()!=0){
			throw new WrongUserFormatException(error.toString());
		}
	}

	public IUser getUserByPseudo(String pseudo) throws DaoException {
		//TODO suppose pseudo unique
		logger.info("[Core] Managing to User with pseudo\""+pseudo+"\"");
		
		
		Iterable<IUser> users = userDao.getSome("pseudo", pseudo);
		if(users == null)
			return null;
		Iterator<IUser> iterator = users.iterator();
		if(iterator.hasNext())
			return iterator.next();
		return null;
	}

	public void addAuthority(String userId, Authority authority) throws InvalidObjectIdException, DaoException {
		logger.info("[Core] Managing to add Authority \""+authority.toString()+"\" to User \""+userId+"\"");
		IUser user = get(userId);
		user.addAuthority(authority);
		try	{
			update(user);
		}catch(Exception e)	{
			//TODO normalement impossible
		}	
	}
}
