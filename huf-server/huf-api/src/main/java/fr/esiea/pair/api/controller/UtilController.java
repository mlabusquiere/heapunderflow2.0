package fr.esiea.pair.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.esiea.pair.api.controller.exception.ControllerException;
import fr.esiea.pair.api.controller.exception.implementation.InvalidLoginException;
import fr.esiea.pair.api.controller.exception.implementation.NeedToBeAuthenticatedException;
import fr.esiea.pair.dao.exception.DaoException;
import fr.esiea.pair.model.exception.RestException;
import fr.esiea.pair.model.implementation.user.User;
import fr.esiea.pair.model.implementation.user.composant.Authority;

  
@Controller
@RequestMapping("/*")
public class UtilController extends ApiExceptionHandlerController {

	@Autowired
	@Qualifier(value = "authenticationManager")
	AuthenticationManager authenticationManager;


	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public void login(@RequestBody User userToLog) throws InvalidLoginException {
		 logger.info("[Controller] Querying to log in User \""+userToLog.toString()+"\"");
		 UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userToLog.getPseudo(), userToLog.getPassword());

		try {
			
			Authentication auth = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(auth);

		} catch (BadCredentialsException ex) {
			throw new InvalidLoginException();
		}
	}
	
	//@Secured("{ROLE_USER,ROLE_ADMIN}")
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public void logout() throws InvalidLoginException, NeedToBeAuthenticatedException, DaoException {
		logger.info("[Controller] Querying to log out User : \""+getUserConnected().toString()+"\"");
		SecurityContext context = SecurityContextHolder.getContext();
		if(context.getAuthentication()!=null)
			SecurityContextHolder.getContext().setAuthentication(null);
	}
	
	@RequestMapping(value  = "")
	@ResponseBody
	public ResponseEntity<RestException> URIError() {

		RestException error = new ControllerException(404, 1025,"URL not found.", null, "/rest/error.jsp");
		return new ResponseEntity<RestException>(error, HttpStatus.NOT_FOUND);

	}

	@RequestMapping(value = "/fun")
	@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
	public void fun()	{

	}
	
	@RequestMapping(value = "/isauthenticated")
	@ResponseBody
	public boolean isAuthenticated() throws DaoException {
		try {
			logger.info("[Controller] Querying to check if User \""+getUserConnected().toString()+"\" is connected");
			if(getUserConnected()!=null)
				return true;
		} catch (NeedToBeAuthenticatedException e) {
			return false;
		}
		return false;
	}
	
	@RequestMapping(value = "/isadmin")
	@ResponseBody
	public boolean isAdmin() throws DaoException {
		try {
			logger.info("[Controller] Querying to check if User \""+getUserConnected().toString()+"\" is admin");
			if(getUserConnected()!=null)
				if(getUserConnected().getAuthorities().contains(new Authority("ROLE_ADMIN")));
		} catch (NeedToBeAuthenticatedException e) {
			return false;
		}
		return false;
	}
}
