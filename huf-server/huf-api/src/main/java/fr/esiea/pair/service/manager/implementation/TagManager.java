package fr.esiea.pair.service.manager.implementation;

import org.apache.log4j.Logger;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.dao.mongo.implementation.DaoSimple;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.service.manager.imanager.implementation.IViewManager;

public class TagManager extends IViewManager<Tag>{

	private DaoFacade<Tag> tagDao;
	public static Logger logger = Logger.getLogger(TagManager.class);

	public TagManager(DaoSimple<Tag> tagDao) {
		super(tagDao);
		this.tagDao = tagDao;
	}

	public Iterable<Tag> getByTag(String tag) throws NoDataBaseConnectionException {
		logger.info("[Core] Managing to get Tag : \""+tag+"\"");
		return tagDao.getSome("tag", tag);	
	}
}
