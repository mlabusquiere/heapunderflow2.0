package fr.esiea.pair.api.controller.exception.implementation;

import fr.esiea.pair.api.controller.exception.ControllerException;

@SuppressWarnings("serial")
public class InvalidLoginException extends ControllerException {
	//TODO correct the message
	public InvalidLoginException(String message) {
		super(400, 10025, null,"/rest/error.jsp", message);
	}

	public InvalidLoginException() {
		super(400, 10025, null,"/rest/error.jsp", null);
	}
}
