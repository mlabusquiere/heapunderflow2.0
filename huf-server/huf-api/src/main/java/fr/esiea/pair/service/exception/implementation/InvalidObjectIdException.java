package fr.esiea.pair.service.exception.implementation;

import org.bson.types.ObjectId;

import fr.esiea.pair.service.exception.ServiceException;

@SuppressWarnings("serial")
public class InvalidObjectIdException extends ServiceException {

    public InvalidObjectIdException(ObjectId id) {
        super(500, 10023, null,"/rest/error.jsp","This id doesn't exist: " + id.toString());

    }

    public InvalidObjectIdException(String string) {
    	   super(500, 10023, null,"/rest/error.jsp",string);
    }
}
