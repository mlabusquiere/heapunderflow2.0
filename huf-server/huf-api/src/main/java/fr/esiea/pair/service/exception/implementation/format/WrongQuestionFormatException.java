package fr.esiea.pair.service.exception.implementation.format;

@SuppressWarnings("serial")
public class WrongQuestionFormatException extends WrongFormatException {

	public WrongQuestionFormatException(String message) {
		super(400, 10026, null,"/rest/error.jsp", message);
	}

	public WrongQuestionFormatException() {
		super(400, 10026, null,"/rest/error.jsp", null);
	}

}

