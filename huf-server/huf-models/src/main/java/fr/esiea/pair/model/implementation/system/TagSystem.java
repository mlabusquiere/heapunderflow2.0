package fr.esiea.pair.model.implementation.system;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.post.composant.Tag;

public abstract class TagSystem extends AnswerSystem  {

	@JsonProperty("tags")
	private SortedSet<IModel> tags;
	
	public TagSystem(String authorId, String authorPseudo, String text) {
		super(authorId,authorPseudo,text);
		innit();
	}

	public TagSystem() {super();}

	public Set<IModel> getTags() {
		return tags;
	}
	
	public void setTags(Set<IModel> tags) {

		this.tags =  new TreeSet<IModel>(tags);
	}

	public void addTag(Tag tag)	{
		innit();
		tags.add(tag);
	}
	
	private void innit() 	{
		if(tags == null)
			tags = new TreeSet<IModel>();
	}
}
