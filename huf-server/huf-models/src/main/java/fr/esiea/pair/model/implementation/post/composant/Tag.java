package fr.esiea.pair.model.implementation.post.composant;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.Model;

public class Tag extends Model implements IModel, Comparable<Tag> {
	

	@JsonProperty("tag")
	private String tag;
	@JsonProperty("description")
	private String description;
	
	public Tag()	{}
	
	public Tag(String tag, String description) {
		super();
		this.tag = tag;
		this.description = description;
	}

	
	public String getTag() {
		return tag;
	}


	public String getDescription() {
		return description;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Tag [tag=" + tag + ", description=" + description + "]";
	}

	@Override
	public int compareTo(Tag o) {
		return this.getTag().compareToIgnoreCase(o.getTag());
	}

}
