package fr.esiea.pair.model.imodel;

import java.util.Set;

import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.composant.Tag;

public interface IQuestion extends IModel,IPost {


	/*** Getters ***/
	public abstract String getTitle();
	public abstract Set<IModel> getVotes();
	public abstract Set<IModel> getAnswers();
	public abstract Set<IModel> getTags();

	/*** Setters ***/
	public abstract void setTitle(String title);
	public abstract void setAnswers(Set<IModel> answers);
	public abstract void setTags(Set<IModel> tags);

	/*** Methods ***/

	public abstract void addAnswer(Answer answer);
	public abstract void addTag(Tag tag);

	public abstract int getReputation();
	public abstract String toString();
	public abstract int compareTo(IPost o);

}