package fr.esiea.pair.model.imodel;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.esiea.pair.model.implementation.post.composant.Vote;

public interface IPost extends IModel{

	public abstract String getAuthorId();
	public abstract String getAuthorPseudo();
	public abstract String getText();
	public abstract Set<IModel> getVotes();
	
	public abstract void setAuthorId(String authorId);
	public abstract void setAuthorPseudo(String authorPseudo);
	public abstract void setText(String text);
	public abstract void setVotes(Set<IModel> votes);
	
	@JsonIgnore
	public abstract int getReputation();
	public abstract void addVote(Vote vote);
}