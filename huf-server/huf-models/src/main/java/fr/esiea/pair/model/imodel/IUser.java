package fr.esiea.pair.model.imodel;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.model.implementation.user.composant.Authority;

public interface IUser extends IModel{

	public abstract String getPseudo();

	public abstract String getPassword();

	public abstract void setPseudo(String userPseudo);

	public abstract void setPassword(String userPassword);

	@JsonIgnore
	public abstract int getReputation();

	public abstract Set<IModel> getVotes();

	public abstract void setVotes(Set<IModel> votes);

	public abstract void setAuthorities(Set<IModel> authorities);

	public abstract void addVote(Vote vote);

	public abstract void addAuthority(Authority authoritie);

	public abstract Set<IModel> getAuthorities();

}