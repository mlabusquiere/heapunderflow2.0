package fr.esiea.pair.model.implementation.user.composant;

import fr.esiea.pair.model.implementation.Model;

public class Authority extends Model {

	private String authority;
	public Authority() {}
	
	
	public Authority(String role) {
		authority = role;
	}
	
//	@Override
//	@JsonIgnore
//	public Map<MongoCollections, Set<IModel>> getDataMap() {
//		Map<MongoCollections, Set<IModel>> map = new HashMap<MongoCollections, Set<IModel>>();
//		return map;
//	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authoritie) {
		this.authority = authoritie;
	}


	@Override
	public String toString() {
		return "Authority [authority=" + authority + "]";
	}

}
