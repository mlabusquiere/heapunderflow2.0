package fr.esiea.pair.model.implementation.post;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IPost;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.model.implementation.system.VoteSystem;

public abstract class  Post extends VoteSystem implements IModel, Comparable<IPost>, IPost  {

	@JsonProperty("authorId")
	private ObjectId authorId;
	@JsonProperty("authorPseudo")
	private String authorPseudo;

	@JsonProperty("text")
	protected String text = null;

	protected Post() {}

	public Post(IUser author, String text)	{
		super();
		this.authorId 		= 	new ObjectId(author.getId());
		this.authorPseudo 	= 	author.getPseudo();

		this.text			=	text;
	}

	public Post(String authorId, String authorPseudo, String text) {
		super();
		this.authorId 		= 	new ObjectId(authorId);
		this.authorPseudo 	= 	authorPseudo;

		this.text		=	text;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#getAuthorId()
	 */
	@Override
	public String getAuthorId() {
		if(authorId == null) {
			return null;
		}
		return authorId.toString();
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#setAuthorId(java.lang.String)
	 */
	@Override
	public void setAuthorId(String authorId) {
		this.authorId = new ObjectId(authorId);
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#getAuthorPseudo()
	 */
	@Override
	public String getAuthorPseudo() {
		return authorPseudo;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#setAuthorPseudo(java.lang.String)
	 */
	@Override
	public void setAuthorPseudo(String authorPseudo) {
		this.authorPseudo = authorPseudo;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#getText()
	 */
	@Override
	public String getText() {
		return text;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#setText(java.lang.String)
	 */
	@Override
	public void setText(String text) {
		this.text = text;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.implementation.post.IPost#getReputation()
	 */
	@Override
	@JsonIgnore
	public int getReputation() {
		int acc=0;
		
		for(IModel model: this.getVotes())	{
			Vote vote = (Vote) model;
			acc+=vote.getReputation()*vote.getCoeff();
		}
		return acc;
	}

	@Override
	public String toString() {
		return "Post [getAuthorId()=" + getAuthorId() + ", getAuthorPseudo()="
				+ getAuthorPseudo() + ", getText()=" + getText()
				+ ", getReputation()=" + getReputation() + super.toString() + "]";
	}

}