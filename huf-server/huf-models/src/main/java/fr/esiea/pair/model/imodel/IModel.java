package fr.esiea.pair.model.imodel;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IModel {

	public abstract String getId();

	public abstract void setId(String id);

	public abstract void generateId();

	public abstract String toJsonString() throws JsonProcessingException;

}