package fr.esiea.pair.model.exception;


@SuppressWarnings("serial")
public abstract class RestException extends Exception {

	private int status;
	private int code;
	private String developerMsg;
	private String moreInfoUrl;
	private String msg;

	
	public RestException(int status, int code, String developerMsg,String moreInfoUrl, String msg) {
		super();
		this.status = status;
		this.code = code;
		this.developerMsg = developerMsg;
		this.moreInfoUrl = moreInfoUrl;
		this.msg = msg;
	}

	public int getStatus() {
		return status;
	}

	public int getCode() {
		return code;
	}

	public String getDeveloperMsg() {
		return developerMsg;
	}

	public String getMoreInfoUrl() {
		return moreInfoUrl;
	}

	public String getMsg() {
		return msg;
	}

	
}