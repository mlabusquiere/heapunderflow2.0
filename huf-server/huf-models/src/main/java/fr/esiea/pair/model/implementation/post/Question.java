package fr.esiea.pair.model.implementation.post;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IPost;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.system.TagSystem;

public class Question extends TagSystem implements IQuestion {

	@JsonProperty("title")
	private String title;

	@SuppressWarnings("unused") //use by serialization
	private Question()	{ super(); }

	public Question(IUser user, String title, String text) {
		super(user.getId(), user.getPseudo(), text);
		this.title 		= 	title;
		
	}

	public Question(String authorId, String authorPseudo, String title,String text)	{
		super(authorId, authorPseudo, text);
		this.title 			= 	title;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.post.IQuestion#getTitle()
	 */

	@Override
	public String getTitle() {
		return title;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.post.IQuestion#setTitle(java.lang.String)
	 */

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.post.IQuestion#compareTo(fr.esiea.pair.model.post.Post)
	 */
	@Override
	public int compareTo(IPost o) {
		if(o.getReputation() > this.getReputation())
			return 1;
		if(o.getReputation() < this.getReputation())
			return -1;
		return 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Question [getTitle()=" + getTitle() + super.toString() + "]";
	}

}
