package fr.esiea.pair.model.implementation.user;

import java.util.HashSet;
import java.util.TreeSet;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.system.AuthoritySystem;

public class User extends AuthoritySystem implements IUser {

    @JsonProperty("pseudo")
	protected String 	pseudo;
    @JsonProperty("password")
	protected String 	password;
    
    protected User() {}

    public User(String pseudo, String password) {
        this.id 		= 	new ObjectId();
        this.pseudo 	= 	pseudo;
        this.password 	= 	password;
        
        this.setAuthorities(new HashSet<IModel>());
        this.setVotes(new TreeSet<IModel>());
    }

    public User(String pseudo, String password,ObjectId id) {
        this.id		 		= 	id;
        this.pseudo 	= 	pseudo;
        this.password 	= 	password;
        
        this.setAuthorities(new HashSet<IModel>());
        this.setVotes(new TreeSet<IModel>());
    }

    
    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#getPseudo()
	 */
    @Override
	public String getPseudo() {
        return pseudo;
    }

    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#getPassword()
	 */
    @Override
	public String getPassword() {
        return password;
    }

    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#setPseudo(java.lang.String)
	 */
    @Override
	public void setPseudo(String userPseudo) {

        this.pseudo = userPseudo;

    }

    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#setPassword(java.lang.String)
	 */
    @Override
	public void setPassword(String userPassword) {

        this.password = userPassword;

    }
    
    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#getReputation()
	 */
    @Override
	@JsonIgnore
	public int getReputation() {

		return this.getVotes().size()+1;

	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.user.IUser#getDataMap()
	 */
//	@Override
//	@JsonIgnore
//	public Map<MongoCollections, Set<IModel>> getDataMap()	{
//		Map<MongoCollections, Set<IModel>> map = new HashMap<MongoCollections, Set<IModel>>();
//		map.put(MongoCollections.VOTES, this.getVotes());
//		map.put(MongoCollections.AUTHORITIES, this.getAuthorities());
//		return map;
//	}
	
}