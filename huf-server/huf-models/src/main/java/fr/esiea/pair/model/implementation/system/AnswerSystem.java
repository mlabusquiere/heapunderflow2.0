package fr.esiea.pair.model.implementation.system;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.Post;

public abstract class AnswerSystem extends Post {
	
	@JsonProperty("answers")
	private SortedSet<IModel> answers;
	
	public AnswerSystem(String authorId, String authorPseudo, String text) {
		super(authorId,authorPseudo,text);
		innit();
	}

	public AnswerSystem() {super();}

	public Set<IModel> getAnswers() {
		return answers;
	}
	
	public void setAnswers(Set<IModel> answers) {
		this.answers =  new TreeSet<IModel>(answers);
	}

	public void addAnswer(Answer answer)	{
		innit();
		answers.add(answer);
	}
	
	private void innit() 	{
		if(answers == null)
			answers = new TreeSet<IModel>();
	}

	@Override
	public String toString() {
		return "AnswerSystem [getAnswers()=" + getAnswers() + super.toString() + "]";
	}
	
	
}
