package fr.esiea.pair.model.implementation.post.composant;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.Model;

public class Vote extends Model implements Comparable<Vote>	{

	@JsonProperty("authorId")
	private ObjectId authorId;
	@JsonProperty("authorPseudo")
	private String authorPseudo;

	@JsonProperty("authorReputation")
	private int authorReputation;
	@JsonProperty("coeff")
	private int coeff;

	private ObjectId refPost;

	//Calendar date;

	@SuppressWarnings("unused") //use by serialization
	private Vote(){}

	public Vote(IUser iUser,String idPost,int coeff) {
		super();
		this.authorId 			= new ObjectId(iUser.getId());
		this.authorPseudo 		= iUser.getPseudo();
		this.authorReputation 	= iUser.getReputation();
		this.coeff 				= getCoeff(coeff);
		this.refPost			= new ObjectId(idPost);

	}

	public Vote(String authorId, String authorPseudo, int authorReputation, String idPost, int coeff) {
		super();
		this.authorId 			= new ObjectId(authorId);
		this.authorPseudo 		= authorPseudo;
		this.authorReputation 	= authorReputation;
		this.coeff 				= getCoeff(coeff);
		this.refPost			= new ObjectId(idPost);

	}

	public String getAuthorId() {
		if(authorId==null)
			return null;
		return authorId.toString();
	}

	public void setAuthorId(String authorId) {
		this.authorId = new ObjectId(authorId);
	}

	public String getAuthorPseudo() {
		return authorPseudo;
	}

	public void setAuthorPseudo(String authorPseudo) {
		this.authorPseudo = authorPseudo;
	}

	public int getAuthorReputation() {
		return authorReputation;
	}

	public void setAuthorReputation(int authorReputation) {
		this.authorReputation = authorReputation;
	}

	public String getRefPost() {
		if(refPost==null)
			return null;
		return refPost.toString();
	}

	public void setRefPost(String refPost) {
		this.refPost = new ObjectId(refPost);
	}

	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}

	private int getCoeff(int coeff) {
		if(coeff<0)
			return -1;
		return 1;
	}

	public int getReputation() {
		return authorReputation;
	}

	public int getCoeff() {
		return coeff;
	}

	@Override
	public int compareTo(Vote o) {
		if(id == null)
			return 1;
		if(this.id.getTime() < o.id.getTime())
			return 1;
		return -1;
	}

	@Override
	public String toString() {
		return "Vote [authorId=" + authorId + ", authorPseudo=" + authorPseudo
				+ ", authorReputation=" + authorReputation + ", coeff=" + coeff
				+ ", refPost=" + refPost + ", id=" + id + "]";
	}

//	@Override
//	@JsonIgnore
//	public Map<MongoCollections, Set<IModel>> getDataMap() {
//		Map<MongoCollections, Set<IModel>> map = new HashMap<MongoCollections, Set<IModel>>();
//		return map;
//	}



}
