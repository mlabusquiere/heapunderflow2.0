package fr.esiea.pair.model.implementation.system;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.Model;
import fr.esiea.pair.model.implementation.post.composant.Vote;

public abstract class VoteSystem extends Model {

	private SortedSet<IModel> votes;
	
	public VoteSystem()	{
		super();
		innit();
	}
	
	public Set<IModel> getVotes() {
		innit();
		return votes;
	}
	
	public void setVotes(Set<IModel> votes) {

		this.votes =  new TreeSet<IModel>(votes);
	}

	public void addVote(Vote vote)	{
		innit();
		votes.add(vote);
	}
	
	private void innit() 	{
		if(votes == null)
			votes = new TreeSet<IModel>();
	}
}
