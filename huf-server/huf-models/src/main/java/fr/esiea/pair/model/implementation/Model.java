package fr.esiea.pair.model.implementation;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.esiea.pair.model.imodel.IModel;


public abstract class Model implements IModel {
	
	@JsonProperty("_id")
    protected ObjectId id;
	public Model()	{ }
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.Imodel#getId()
	 */
	@Override
	public final String getId() {
    	
    	if(this.id == null)	{
    		return null;
    	}
    	
        return id.toString();
    
    }
	
    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.Imodel#setId(java.lang.String)
	 */
    @Override
	public final void setId(String id) {
    		
		if(id==null)
			this.id=null;
		else
			this.id = new ObjectId(id);
    }
	
    /* (non-Javadoc)
	 * @see fr.esiea.pair.model.Imodel#generateId()
	 */
    @Override
	public final void generateId()	{
    	if(id==null)
			this.id= new ObjectId();
    	
    }
    
	/* (non-Javadoc)
	 * @see fr.esiea.pair.model.Imodel#toJsonString()
	 */
	@Override
	public final String toJsonString() throws JsonProcessingException	{
    	
    	String Data=null;
    	ObjectMapper mapper= new ObjectMapper();
		
    	Data = mapper.writeValueAsString(this);

    	return Data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
//	/* (non-Javadoc)
//	 * @see fr.esiea.pair.model.Imodel#getDataMap()
//	 */
//	@Override
//	@JsonIgnore
//	public abstract Map<MongoCollections, Set<IModel>> getDataMap();
}