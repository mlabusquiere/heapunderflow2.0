package fr.esiea.pair.model.implementation.post;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.model.imodel.IPost;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;

public class Answer extends Post {
	
	@JsonProperty("refQuestion")
	ObjectId refQuestion;

	@SuppressWarnings("unused") //use by serialization
	private Answer(){ }

	public Answer(IUser user, String text,IQuestion question) {
		super(user, text);
		this.refQuestion	= new ObjectId(question.getId());
	}

	
	public Answer(String authorId, String authorPseudo, String text, String refQuestion) {
		super(authorId,authorPseudo,text);
		this.refQuestion	= new ObjectId(refQuestion);
	}

	public Answer(IUser user, String text, String refQuestion) {
		super(user,text);
		this.refQuestion	= new ObjectId(refQuestion);
	}

	public String getRefQuestion() {
		if(refQuestion==null)
			return null;
		return refQuestion.toString();
	}

	public void setRefQuestion(String refQuestion) {
		this.refQuestion = new ObjectId(refQuestion);
	}

	@Override
	public int compareTo(IPost o) {
		if(o == this)
			return 0;
		if(o.getReputation() > this.getReputation())
			return 1;
		return -1;
	}

	@Override
	public String toString() {
		return "Answer [getRefQuestion()=" + getRefQuestion()
				+ ", getAuthorId()=" + getAuthorId() + ", getAuthorPseudo()="
				+ getAuthorPseudo() + ", getText()=" + getText()
				+ ", getReputation()=" + getReputation() + ", getVotes()="
				+ getVotes() + ", getId()=" + getId() + ", hashCode()=" + hashCode()
				+ ", getClass()=" + getClass() + ", toString()="
				+ super.toString() + "]";
	}

}
