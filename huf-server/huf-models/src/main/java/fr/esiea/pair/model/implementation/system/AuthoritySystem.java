package fr.esiea.pair.model.implementation.system;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.user.composant.Authority;

public abstract class AuthoritySystem extends VoteSystem {

	private SortedSet<IModel> Authorities;
	
	public AuthoritySystem()	{
		super();
		innit();
	}
	
	public Set<IModel> getAuthorities() {
		return Authorities;
	}
	
	public void setAuthorities(Set<IModel> Authorities) {

		this.Authorities =  new TreeSet<IModel>(Authorities);
	}

	public void addAuthority(Authority authority)	{
		innit();
		Authorities.add(authority);
	}
	
	private void innit() 	{
		if(Authorities == null)
			Authorities = new TreeSet<IModel>();
	}
	
	
}
