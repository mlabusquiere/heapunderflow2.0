package fr.esiea.pair.dao.mongo.composedobject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.implementation.post.Question;

public class QuestionDaoModel extends ModelFacade implements DaoModel<IQuestion> {
	
	@JsonProperty("authorId") // Pourquoi les autres DaoModel ont pas de json property?
	private ObjectId authorId;
	@JsonProperty("authorPseudo")
	private String authorPseudo; // Useless
	
	@JsonProperty("title")
	private String title;
	@JsonProperty("text")
	private String text;
	
	@JsonProperty("answersRef")
	private Set<ObjectId> answersRef;
	@JsonProperty("votesRef")
	private Set<ObjectId> votesRef;
	@JsonProperty("tagsRef")
	private Set<ObjectId> tagsRef;
	
	public QuestionDaoModel() {}

	public QuestionDaoModel(IQuestion data) {
		setData(data);
	}

	@Override
	public IQuestion getModel(Map<MongoCollections, Set<IModel>> map) {
		IQuestion question = new Question(this.authorId.toString(), this.authorPseudo, this.title, this.text);
		question.setId(id.toString());
		
		if(map.containsKey(MongoCollections.ANSWERS)) {
			question.setAnswers(map.get(MongoCollections.ANSWERS));
		}
		if(map.containsKey(MongoCollections.VOTES)) {
			question.setVotes(map.get(MongoCollections.VOTES));
		}
		if(map.containsKey(MongoCollections.TAGS)) {
			question.setTags(map.get(MongoCollections.TAGS));
		}
		return question;
	}


	@Override
	public Map<MongoCollections, Set<ObjectId>> getRefMap() {
		Map<MongoCollections, Set<ObjectId>> map = new HashMap<MongoCollections, Set<ObjectId>>();
		
		map.put(MongoCollections.ANSWERS, answersRef);
		map.put(MongoCollections.VOTES, votesRef);
		map.put(MongoCollections.TAGS, tagsRef);
		
		return map;
	}

	@Override
	public void setData(IQuestion data) {
		this.id	= new ObjectId(data.getId());
		this.authorId = new ObjectId(data.getAuthorId());
		this.authorPseudo = data.getAuthorPseudo();
		this.text = data.getText();
		this.title = data.getTitle();
		
		this.answersRef = new HashSet<ObjectId>();
		this.votesRef = new HashSet<ObjectId>();
		this.tagsRef = new HashSet<ObjectId>();
		
		Set<IModel> votes = data.getVotes();
		if(votes != null) {
			for(IModel vote :votes) {	
				this.votesRef.add(new ObjectId(vote.getId()));
			}
		}
		
		Set<IModel> answers = data.getAnswers();
		if(answers != null) {
			for(IModel answer :answers) {
				this.answersRef.add(new ObjectId(answer.getId()));
			}
		}
		
		Set<IModel> tags = data.getTags();
		if(tags != null) {
			for(IModel tag :tags) {
				this.tagsRef.add(new ObjectId(tag.getId()));
			}
		}

	}


}
