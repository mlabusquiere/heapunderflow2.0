package fr.esiea.pair.dao.mongo.implementation;

import static org.jongo.MongoCollection.MONGO_QUERY_OID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import com.mongodb.MongoException;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.dao.mongo.composedobject.DaoModel;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.daofacade.SimpleObjectDaoFactory;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.system.AnswerSystem;
import fr.esiea.pair.model.implementation.system.AuthoritySystem;
import fr.esiea.pair.model.implementation.system.TagSystem;
import fr.esiea.pair.model.implementation.system.VoteSystem;


public class DaoComplexe<MODELTYPE extends IModel, DAOMODELTYPE extends DaoModel<MODELTYPE>> implements DaoFacade<MODELTYPE> {
	
	protected MongoCollection collection;
	private Class<DAOMODELTYPE> clazz;
	public static Logger logger = Logger.getLogger(DaoComplexe.class);
	
	public DaoComplexe(MongoCollections collection, Class<DAOMODELTYPE> clazz) throws NoDataBaseConnectionException	{
		this.collection = MongoConnection.getConnection().getCollection(collection.toString());
		this.clazz=clazz;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getOne(org.bson.types.ObjectId)
	 */

	@Override
	public MODELTYPE getOne(ObjectId id) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Getting object with id : \""+id.toString()+"\" from "+collection.toString()+"");
		MODELTYPE data = null;
		
		try	{
			DAOMODELTYPE daoData = collection.findOne(id).as(clazz);
	
			if(daoData==null)
				return null;
			data = DaoModelToModel(daoData);

		}catch(MongoException e)	{
			
			throw new NoDataBaseConnectionException();
			
		}
	
		return data;

	}


	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getAll()
	 */

	@Override
	public Iterable<MODELTYPE> getAll() throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Getting all objects from "+collection.toString()+"");
		Iterable<MODELTYPE> datas;
		
		try	{
			Iterable<DAOMODELTYPE> daoData = collection.find().as(clazz);
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;

	}


	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getSome(java.lang.String)
	 */

	@Override
	public Iterable<MODELTYPE> getSome(String query) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Getting objects from "+collection.toString()+" with query : \""+query+"\"");
		Iterable<MODELTYPE> datas;
		try	{
			Iterable<DAOMODELTYPE> daoData = collection.find(query).as(clazz);
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;

	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getSome(java.lang.String, int, int)
	 */

	@Override
	public Iterable<MODELTYPE> getSome(String query, int skip, int limit) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Getting objects from "+collection.toString()+" with query : \""+query+"\" starting at : \""+skip+"\" limit : \""+limit+"\"");
		Iterable<MODELTYPE> datas;
		try	{
			Iterable<DAOMODELTYPE> daoData = collection.find(query).skip(skip).limit(limit).as(clazz);
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getSome(java.lang.String, org.bson.types.ObjectId)
	 */

	@Override
	public Iterable<MODELTYPE> getSome(String field, ObjectId id) throws NoDataBaseConnectionException	{
		
		Iterable<MODELTYPE> datas;
		try	{
			Iterable<DAOMODELTYPE> daoData = collection.find("{"+field+": {" + MONGO_QUERY_OID + ":\""+id.toString() + "\"}}").as(clazz);
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#getSome(java.lang.String, org.bson.types.ObjectId, int, int)
	 */

	@Override
	public Iterable<MODELTYPE> getSome(String field, ObjectId id, int skip, int limit) throws NoDataBaseConnectionException {
	
		Iterable<MODELTYPE> datas;
		try	{
			Iterable<DAOMODELTYPE> daoData = collection.find("{"+field+": {" + MONGO_QUERY_OID + ":\""+id.toString() + "\"}}").skip(skip).limit(limit).as(clazz);
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;
		
	}

	final protected Iterable<MODELTYPE> DaoModelToModel(Iterable<DAOMODELTYPE> datasToConvert) throws NoDataBaseConnectionException {
		List<MODELTYPE> datas 	=	 new ArrayList<MODELTYPE>();

		for(DAOMODELTYPE daoModelToConvert : datasToConvert)
			datas.add(DaoModelToModel(daoModelToConvert));

		return datas;
	}

	private MODELTYPE DaoModelToModel(DAOMODELTYPE dataModelToConvert) throws NoDataBaseConnectionException {
		//		refMap est la map contenant tous les set de data en objectId
		//		ketSet sont tout les types de data qui sont les clefs de refMap
		//		KeySetIterator est un iterator sur toute les clefs de refMap
		//		dataMap est la Map qui contient les set des reel données
		//REf innit
		
		Map<MongoCollections, Set<ObjectId>> refMap = dataModelToConvert.getRefMap();
		Set<MongoCollections> keySet = refMap.keySet();
		Iterator<MongoCollections> keySetIterator = keySet.iterator();
		//Real data innit
		Map<MongoCollections, Set<IModel>> dataMap = new HashMap<MongoCollections, Set<IModel>>();
		
		while(keySetIterator.hasNext())	{
			MongoCollections collectionName =	keySetIterator.next();
			DaoFacade<?> dao = SimpleObjectDaoFactory.getImplementation(collectionName);
			Set<ObjectId> datasRef = refMap.get(collectionName);
			// congig.getClass
			
			Set<IModel> datas = new HashSet<IModel>();
			try {
				
				for(ObjectId ref:datasRef)
					datas.add(dao.getOne(ref));

			} catch (java.lang.NullPointerException e) {
				// bugg
			} catch (java.lang.IllegalArgumentException e) {
				// the is no datas
			}
			dataMap.put(collectionName, datas);
		}
		
		MODELTYPE model = (MODELTYPE) dataModelToConvert.getModel(dataMap);
		return model;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#save(fr.esiea.pair.model.Model)
	 */

	
	@SuppressWarnings("unchecked")
	@Override
	public void save(MODELTYPE data) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Saving object : \""+data.toString()+"\" in "+collection.toString()+"");
		data.generateId();

		Map<MongoCollections, Set<IModel>> dataMap 	= getDataMap(data);
		Set<MongoCollections> keySet = dataMap.keySet();
		Iterator<MongoCollections> keySetIterator = keySet.iterator();
		
		while(keySetIterator.hasNext())	{
			MongoCollections collectionName =	keySetIterator.next();
			Set<IModel> datasToSave = dataMap.get(collectionName);
			DaoFacade<IModel> dao = (DaoFacade<IModel>)SimpleObjectDaoFactory.getImplementation(collectionName);
			if(datasToSave != null)	{
				for(IModel dataToSave:datasToSave)	{
					dataToSave.generateId();
					dao.save(dataToSave);
				}
			}
				
		}
		
		DAOMODELTYPE daoModel;
		try {
			daoModel = clazz.newInstance();
			((DaoModel<IModel>) daoModel).setData(data);
			
			collection.save(daoModel);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	}

	
	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#remove(org.bson.types.ObjectId)
	 */
	@Override
	public void remove(ObjectId id) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Remove object with id \""+id.toString()+"\" from "+collection.toString()+"");
		MODELTYPE data = getOne(id);
		

		Map<MongoCollections, Set<IModel>> dataMap = getDataMap(data);
		Iterator<MongoCollections> keySetIterator = dataMap.keySet().iterator();
		
		while(keySetIterator.hasNext())	{
			MongoCollections collectionName =	keySetIterator.next();
			Set<IModel> datasToRemove = dataMap.get(collectionName);
			DaoFacade<?> dao = SimpleObjectDaoFactory.getImplementation(collectionName);
			
			for(IModel dataToSave:datasToRemove)
				dao.remove(new ObjectId(dataToSave.getId()));
				
		}
		
		collection.remove(id);
	}

	/* (non-Javadoc)
	 * @see fr.esiea.pair.dao.mongo2.DaoFacade#insert(ModelType)
	 */

	@Override
	public void insert(MODELTYPE data) throws NoDataBaseConnectionException {
		logger.info("[MongoDB] Saving (insert) object : \""+data.toString()+"\" in "+collection.toString()+"");
		save(data); //as far that save and insert is the same with jongo
		
	}

	@Override
	public Iterable<MODELTYPE> getSome(String field, String fieldContent) throws NoDataBaseConnectionException {
		Iterable<MODELTYPE> datas;
		try	{

			Iterable<DAOMODELTYPE> daoData = collection.find("{" + field+ ":\""+fieldContent + "\"}").as(clazz);
			
			if(!daoData.iterator().hasNext())
				return null;
			datas = DaoModelToModel(daoData);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;
	}

	private Map<MongoCollections, Set<IModel>> getDataMap(MODELTYPE data) {
		Map<MongoCollections,Set<IModel>> map = new HashMap<MongoCollections,Set<IModel>>();
		if(data instanceof VoteSystem)
			map.put(MongoCollections.VOTES, ((VoteSystem)data).getVotes());
		if(data instanceof TagSystem)
			map.put(MongoCollections.TAGS,((TagSystem)data).getTags());
		if(data instanceof AnswerSystem)
			map.put(MongoCollections.ANSWERS, ((AnswerSystem)data).getAnswers());
		if(data instanceof AuthoritySystem)
			map.put(MongoCollections.AUTHORITIES, ((AuthoritySystem)data).getAuthorities());
			
		return map;
	}
}
