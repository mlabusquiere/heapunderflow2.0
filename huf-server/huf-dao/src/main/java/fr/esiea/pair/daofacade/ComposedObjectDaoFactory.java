package fr.esiea.pair.daofacade;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.dao.mongo.composedobject.AnswerDaoModel;
import fr.esiea.pair.dao.mongo.composedobject.QuestionDaoModel;
import fr.esiea.pair.dao.mongo.composedobject.UserDaoModel;
import fr.esiea.pair.dao.mongo.implementation.DaoComplexe;
import fr.esiea.pair.model.imodel.IQuestion;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.post.Answer;

public class ComposedObjectDaoFactory {
	
	public static DaoFacade<Answer> getAnswerDaoImplementation() throws NoDataBaseConnectionException {
		return new DaoComplexe<Answer, AnswerDaoModel>(MongoCollections.ANSWERS, AnswerDaoModel.class);
	}
	
	public static DaoFacade<IQuestion> getQuestionDaoImplementation() throws NoDataBaseConnectionException {
		return new DaoComplexe<IQuestion, QuestionDaoModel>(MongoCollections.QUESTIONS, QuestionDaoModel.class);
	}
	
	public static DaoFacade<IUser> getUserDaoImplementation() throws NoDataBaseConnectionException {
		return new DaoComplexe<IUser, UserDaoModel>(MongoCollections.USERS, UserDaoModel.class);
	}
	
}
