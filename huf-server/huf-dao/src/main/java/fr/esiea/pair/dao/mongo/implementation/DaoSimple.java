package fr.esiea.pair.dao.mongo.implementation;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import com.mongodb.MongoException;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.daofacade.DaoFacade;
import fr.esiea.pair.model.imodel.IModel;

public class DaoSimple<ModelType extends IModel> implements DaoFacade<ModelType>{

	protected MongoCollection collection;
	private Class<ModelType> clazz;
	public static Logger logger = Logger.getLogger(DaoSimple.class);

	public DaoSimple(MongoCollections collection, Class<ModelType> clazz) throws NoDataBaseConnectionException	{
		this.collection = MongoConnection.getConnection().getCollection(collection.toString());
		this.clazz=clazz;
	}

	@Override
	public ModelType getOne(ObjectId id) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Getting object with id : \""+id.toString()+"\" from "+collection.toString()+"");
		ModelType daoModel = null;


		try	{
			daoModel = collection.findOne(id).as(clazz);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		if(daoModel == null)
			return null;
		return daoModel;

	}



	@Override
	public Iterable<ModelType> getAll() throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Getting all objects from "+collection.toString()+"");
		Iterable<ModelType> datas;
		try	{
			datas = collection.find().as(clazz);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;

	}



	@Override
	public Iterable<ModelType> getSome(String query) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Getting objects from "+collection.toString()+" with query : \""+query+"\"");
		Iterable<ModelType> datas;
		try	{
			datas = collection.find(query).as(clazz);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;

	}


	@Override
	public Iterable<ModelType> getSome(String query, int skip, int limit) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Getting objects from "+collection.toString()+" with query : \""+query+"\" starting at : \""+skip+"\" limit : \""+limit+"\"");
		Iterable<ModelType> datas;
		try	{
			datas = collection.find(query).skip(skip).limit(limit).as(clazz);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
		return datas;

	}

	@Override
	public void save(ModelType data) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Saving object : \""+data.toString()+"\" in "+collection.toString()+"");
		try	{	
			collection.save(data);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
	}

	@Override
	public void remove(ObjectId id) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Remove object with id \""+id.toString()+"\" from "+collection.toString()+"");
		try	{		
			collection.remove(id);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
	}

	@Override
	public void insert(ModelType data) throws NoDataBaseConnectionException {
		logger.debug("[MongoDB] Saving (insert) object : \""+data.toString()+"\" in "+collection.toString()+"");
		try	{
			collection.save(data);
		}catch(MongoException e)	{
			throw new NoDataBaseConnectionException();
		}
	}

	@Override
	public Iterable<ModelType> getSome(String field, ObjectId id)
			throws NoDataBaseConnectionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<ModelType> getSome(String field, ObjectId id, int skip,
			int limit) throws NoDataBaseConnectionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<ModelType> getSome(String field, String fieldContent)
			throws NoDataBaseConnectionException {
		// TODO Auto-generated method stub
		return null;
	}


}
