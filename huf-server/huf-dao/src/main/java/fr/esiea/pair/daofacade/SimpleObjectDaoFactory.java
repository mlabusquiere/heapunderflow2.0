package fr.esiea.pair.daofacade;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.dao.mongo.composedobject.AnswerDaoModel;
import fr.esiea.pair.dao.mongo.implementation.DaoComplexe;
import fr.esiea.pair.dao.mongo.implementation.DaoSimple;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.post.Answer;
import fr.esiea.pair.model.implementation.post.composant.Tag;
import fr.esiea.pair.model.implementation.post.composant.Vote;
import fr.esiea.pair.model.implementation.user.composant.Authority;

public class SimpleObjectDaoFactory {
	
	public static DaoFacade<? extends IModel> getImplementation(MongoCollections collection) throws NoDataBaseConnectionException {

			if(collection.equals(MongoCollections.VOTES)) {
				return new DaoSimple<Vote>(collection, Vote.class);
			}
			if(collection.equals(MongoCollections.ANSWERS)) {
				return new DaoComplexe<Answer, AnswerDaoModel>(MongoCollections.ANSWERS, AnswerDaoModel.class);
				}
			if(collection.equals(MongoCollections.TAGS)) {
				return new DaoSimple<Tag>(collection, Tag.class);
			}
			if(collection.equals(MongoCollections.AUTHORITIES)) {
				return new DaoSimple<Authority>(collection, Authority.class);
			}
			System.out.println("bugg in the dao factory");
			return null;
	}
}
