package fr.esiea.pair.dao.mongo.composedobject;

import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.model.imodel.IModel;

public interface DaoModel<E extends IModel> {

	public abstract E getModel(Map<MongoCollections, Set<IModel>> dataMap);
	public abstract Map<MongoCollections, Set<ObjectId>> getRefMap();
	public abstract void setData(E model);
	public abstract void generateId();
	
}
