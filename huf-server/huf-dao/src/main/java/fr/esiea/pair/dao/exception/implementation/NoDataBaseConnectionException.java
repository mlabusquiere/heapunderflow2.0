package fr.esiea.pair.dao.exception.implementation;

import fr.esiea.pair.dao.exception.DaoException;

@SuppressWarnings("serial")
public class NoDataBaseConnectionException extends DaoException {
	//TODO correct the message
	public NoDataBaseConnectionException(String message) {
		super(400, 10025, null,"/rest/error.jsp", message);
	}

	public NoDataBaseConnectionException() {
		super(400, 10025, null,"/rest/error.jsp", null);
	}

}
