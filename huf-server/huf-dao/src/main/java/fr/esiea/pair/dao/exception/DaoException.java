package fr.esiea.pair.dao.exception;

import fr.esiea.pair.model.exception.RestException;

@SuppressWarnings("serial")
public class DaoException extends RestException {
	
	public DaoException(int status, int code, String developerMsg, String moreInfoUrl, String msg) {
		super(status, code, developerMsg, moreInfoUrl, msg);
	}
	
}
