package fr.esiea.pair.dao.mongo.collection;

public enum MongoCollections {
	ANSWERS ("Answers"),
	QUESTIONS ("Questions"),
	TAGS ("Tags"),
	USERS ("Users"),
	VOTES ("Votes"),
	AUTHORITIES ("Authorities");
	
	private String collection = "";
	
	MongoCollections(String collection) {
		this.setCollection(collection);
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}
	
	public String toString(){
	    return collection;
	}
}
