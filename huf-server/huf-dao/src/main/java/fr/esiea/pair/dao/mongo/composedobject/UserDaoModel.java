package fr.esiea.pair.dao.mongo.composedobject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.imodel.IUser;
import fr.esiea.pair.model.implementation.user.User;

public class UserDaoModel extends ModelFacade implements DaoModel<IUser> {
	
	private String password;
	private String pseudo;
	
	private Set<ObjectId> votesRef;
	private Set<ObjectId> authoritiesRef;

	public UserDaoModel() {}

	public UserDaoModel(User data) {
		setData(data);
	}

	
	@Override
	public IUser getModel(Map<MongoCollections, Set<IModel>> map) {
		
		User user = new User(pseudo, password);
		
		user.setId(this.id.toString());
		
		if(map.containsKey(MongoCollections.VOTES)) {
			user.setVotes(map.get(MongoCollections.VOTES));
		}
		
		if(map.containsKey(MongoCollections.AUTHORITIES)) {
			user.setAuthorities(map.get(MongoCollections.AUTHORITIES));
		}
		
		return user;
		
	}


	@Override
	public Map<MongoCollections, Set<ObjectId>> getRefMap() {
		Map<MongoCollections, Set<ObjectId>> map = new HashMap<MongoCollections, Set<ObjectId>>();
		
		map.put(MongoCollections.VOTES, votesRef);
		map.put(MongoCollections.AUTHORITIES,authoritiesRef);
		
		return map;
	
	}

	@Override
	public void setData(IUser data) {
		
		data.generateId();
		
		this.id 				= new ObjectId(data.getId());
		this.password 			= data.getPassword();
		this.pseudo 			= data.getPseudo();
		
		this.authoritiesRef 	= new HashSet<ObjectId>();
		this.votesRef			= new HashSet<ObjectId>();
		
		Set<IModel> votes 		= data.getVotes();
		Set<IModel> authorities = data.getAuthorities();
		
		if(votes !=null) {
			for(IModel vote :votes) {
				this.votesRef.add(new ObjectId(vote.getId()));
			}
		}
		
		if(authorities !=null) {
			for(IModel authoritie : authorities) {
				this.authoritiesRef.add(new ObjectId(authoritie.getId()));
			}
		}
		
	}

}
