package fr.esiea.pair.dao.mongo.composedobject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import fr.esiea.pair.dao.mongo.collection.MongoCollections;
import fr.esiea.pair.model.imodel.IModel;
import fr.esiea.pair.model.implementation.post.Answer;

public class AnswerDaoModel extends ModelFacade implements DaoModel<Answer>{

	private ObjectId refQuestion;
	
	private ObjectId authorId;
	private String authorPseudo; // Useless
	
	private String text;
	
	private Set<ObjectId> votesRef;

	public AnswerDaoModel() {}

	public AnswerDaoModel(Answer data) {
		
		setData(data);
	
	}

	@Override
	public Answer getModel(Map<MongoCollections, Set<IModel>> map) {
		
		Answer answer = new Answer(this.authorId.toString(), this.authorPseudo, this.text, this.refQuestion.toString());
		answer.setId(id.toString());
		
		if(map.containsKey(MongoCollections.VOTES)) {
			answer.setVotes(map.get(MongoCollections.VOTES));
		}
		
		return answer;
	}


	@Override
	public Map<MongoCollections, Set<ObjectId>> getRefMap() {
		
		Map<MongoCollections, Set<ObjectId>> map = new HashMap<MongoCollections, Set<ObjectId>>();
		
		map.put(MongoCollections.VOTES,votesRef);
		
		return map;
	
	}

	@Override
	public void setData(Answer data) {
		
		this.id	= new ObjectId(data.getId());
		this.refQuestion = new ObjectId(data.getRefQuestion());
		this.authorId = new ObjectId(data.getAuthorId());
		this.authorPseudo = data.getAuthorPseudo();
		this.text = data.getText();
		
		this.votesRef = new HashSet<ObjectId>();
		
		Set<IModel> votes = data.getVotes();
		
		if(votes != null) {
			for(IModel vote : votes) {	
				this.votesRef.add(new ObjectId(vote.getId()));
			}
		}
	}

}
