package fr.esiea.pair.daofacade;

import org.bson.types.ObjectId;

import fr.esiea.pair.dao.exception.implementation.NoDataBaseConnectionException;
import fr.esiea.pair.model.imodel.IModel;

public interface DaoFacade<ModelType extends IModel> {

	public abstract ModelType getOne(ObjectId id)
			throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getAll()
			throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getSome(String query)
			throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getSome(String query, int skip,
			int limit) throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getSome(String field, ObjectId id)
			throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getSome(String field, ObjectId id,
			int skip, int limit) throws NoDataBaseConnectionException;

	public abstract Iterable<ModelType> getSome(String field,String fieldContent)
			throws NoDataBaseConnectionException;
	
	public abstract void save(ModelType dataToSave) throws NoDataBaseConnectionException;

	public abstract void remove(ObjectId id)
			throws NoDataBaseConnectionException;

	public abstract void insert(ModelType data)
			throws NoDataBaseConnectionException;
	
	

}